# BindSpace
TFBS models trained with HT-SELEX data using StarSpace embedding method

## installation

### install dependencies
To install this R packages, please make sure you have installed the most updated versions of the following packages:  
- data.table  
- Matrix  
- Biostrings  
- parallel  
- glmnet  

### download BindSpace to local directory
git clone https://bitbucket.org/hy395/bindspace.git

The model file is quite large (~240MB). Check md5sum of bindspace/inst/extdata/model_bindspace.rds to see if you correctly downloaded the model file (md5sum: 3be69525f710ee948b16d737f0b8c770).   
If the model file is broken, try downloading it manually here:
https://bitbucket.org/hy395/bindspace/src/master/inst/extdata/model_bindspace.rds  
or here:
https://drive.google.com/open?id=1y7Wsph4ocUw9uidPDfXqFykFw1CD1yse  
and save to:
bindspace/inst/extdata/model_bindspace.rds  

### install in R
install.packages("bindspace/", repos=NULL, type="source")  

### additional packages will be required for running the scripts in tests:
- ComplexHeatmap
- Rtsne
- ggplot2
- ggrepel
- circlize
- scales

## test
example code for generate t-SNE embedding of learnt model: bindspace/tests/tsne_embedding.R  
example code for making predictions with BindSpace and BindSpace+ models: bindspace/tests/test_package.R
