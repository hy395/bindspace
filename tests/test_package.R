library(BindSpace)
library(ComplexHeatmap)
library(circlize)

model.path <- system.file("extdata","model_bindspace.rds" ,package="BindSpace")
ridge.path <- system.file("extdata","model_bindspaceplus_1vall.rds" ,package="BindSpace")
chip.path <- system.file("extdata", "test_MAFK_ChIP.fasta", package="BindSpace")

model <- readRDS(model.path)
ridge <- readRDS(ridge.path)
chip <- readDNAStringSet(chip.path)

#############################
# prediction on 20bp probes #
#############################
# this is just a demonstration for using the model to make predictions.
# the model used here is trained on all HT-SELEX data.
# so the performance here is NOT the same as the reported performance on hold-out probes.

# 1) make prediction
load(system.file("extdata", "htselex.rdt", package="BindSpace"))
scores <- eval_probe(train_50, model) # prediction scores for each probe for each TF

# 2) compute confusion matrix
scores <- scores[,-grep("#Family",colnames(scores))]
prediction <- colnames(scores)[apply(scores, 1, which.max)]
prediction <- gsub("#|TF_","",prediction)
labels <- sort(unique(labels_50$tf))
confusion <- matrix(0, length(labels), length(labels))
for (i in 1:length(labels)) {
  for (j in 1:length(labels)) {
    confusion[i,j] <- sum (labels_50$tf==labels[i] & prediction==labels[j])
  }
}
rownames(confusion) <- colnames(confusion) <- labels
confusion_norm <- confusion/rowSums(confusion)

# 3) plot confusion matrix for selected family
family <- "Ets"
family_members <- unique(labels_50[labels_50$family==family,"tf"])
select <- which(rownames(confusion_norm) %in% family_members)
ht <- Heatmap(confusion_norm[select,select], col = colorRamp2(c(0, 0.25, 0.5, 0.75, 1), c("#2c7bb6","#abd9e9" , "#ffffbf","#fdae61","#d7191c")),
              cluster_rows = FALSE, cluster_columns = FALSE)
draw(ht)

######################################
# predict on ChIP-seq with BindSpace #
######################################
res2 <- eval_peak(chip, model, "MAFK")

# 1. score table shows predicted score for all TFs for each peak
a <- res2$score_table
sort(table(colnames(a)[apply(a,1,which.max)])) # 1621/2000 peaks are predicted to be MAFK peak

# 2. TF_score shows predicted score of MAFK across each ChIP-seq peak
b <- res2$TF_score
ht1 <- Heatmap(b, cluster_columns=F, cluster_rows=F, show_row_names=F, show_heatmap_legend=F, show_column_names=F,
               col=colorRamp2(c(0, max(b)/2, max(b)), c("blue","white", "red")))
draw(ht1)

###########################
# predict with BindSpace+ #
###########################
res3 <- eval_peak_bindspaceplus(chip, model, ridge[c("MAFK","ATF4","CEBPB")])

