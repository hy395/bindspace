library(BindSpace)
library(Rtsne)
library(ggplot2)
library(ggrepel)
library(scales)

######################################
# T-SNE embedding of training probes #
######################################
dir.create("tsne")

# 1) load data
model.path <- system.file("extdata","model_bindspace.rds" ,package="BindSpace")
model <- readRDS(model.path)
htselex <- system.file("extdata","htselex.rdt" ,package="BindSpace")
load(htselex) # load two objects: train_50 and labels_50. 50 probes and labels for each TF in HTSELEX.

# 2) embed these probes to BindSpace
train_embed <- embed_probe(train_50, model)
rownames(train_embed) <- paste0("probe_", make.unique(labels_50$family))

# 3) t-sne embed
to_tsne <- data.matrix(rbind(train_embed, model$label_embed))
dot_sim <- to_tsne%*%t(to_tsne)
dot_sim <- exp(-dot_sim) # convert similarity to distance
tsne <- Rtsne(dot_sim, dims = 2, is_distance=T, verbose=T, check_duplicates=F)
embed <- tsne$Y
rownames(embed) <- rownames(to_tsne)
embed <- data.frame(embed)

# 4) plot TSNE embedding
probes <- embed[grep("probe_",rownames(embed)),]
tfs <-  embed[grep("#TF",rownames(embed)),]
family <-  embed[grep("#Family",rownames(embed)),]

# plot #1: 13 major families
toplot1 <- probes
toplot1$color <- gsub("probe_|\\..*","",rownames(toplot1))

info <- unique(labels_50[,2:3])
large_families <- sapply(split(info$tf, info$family), function(x) length(unique(x)))
choose <- names(large_families[large_families > 3])
choose <- choose[choose!="Unknown"]
toplot1 <- toplot1[toplot1$color%in%choose,]
toplot2 <- family[gsub("#Family_","",rownames(family))%in%choose,]
toplot2$color <- "labels"
toplot <- rbind(toplot1, toplot2)
toplot$color <- factor(toplot$color, levels = c(unique(toplot1$color), "labels"))

colors <- c(hue_pal()(13),"black")
names(colors) <- levels(toplot$color)
pdf("tsne/large_families.pdf", 12, 10)
ggplot() + geom_point(data = toplot, aes(X1, X2, color=color)) +
  geom_text_repel(data = toplot2, aes(X1, X2, label = gsub("#TF_|#Family_","",rownames(toplot2)), color=color), size = 5, fontface="bold") +
  scale_color_manual(values=colors) +
  labs(x="dim1", y="dim2", title="all TFs") + theme_classic()
dev.off()

# plot #2: all labels
toplot2 <- rbind(tfs, family)
toplot2$color <- "label_tf"
toplot2[grep("#Family_", rownames(toplot2)), "color"] <- "label_family"
toplot2[gsub("#TF_","",rownames(toplot2)) %in% info[info$family=="Homeodomain",]$TF, "color"] <- "label_Homeodomain_tf"
toplot <- rbind(toplot1, toplot2)
toplot$color <- factor(toplot$color, levels=c(unique(toplot1$color),"label_tf","label_Homeodomain_tf","label_family"))
colors <- c(hue_pal()(13), "black","gray","red")
names(colors) <- levels(toplot$color)
tolabel <- toplot2[toplot2$color!="label_Homeodomain_tf",]
tolabel$color <- factor(tolabel$color, levels=levels(toplot$color))
pdf("tsne/all_TFs.pdf", 12, 10)
ggplot() + geom_point(data = toplot, aes(X1, X2, color=color)) +
  geom_text_repel(data = tolabel, aes(X1, X2, label = gsub("#TF_|#Family_","",rownames(tolabel)), color=color), 
                  size = 2.5, fontface="bold") +
  scale_color_manual(values=colors) +
  labs(x="dim1", y="dim2", title="all TFs") + theme_classic()
dev.off()


